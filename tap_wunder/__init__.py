#!/usr/bin/env python3
import os
import json
import singer
import urllib.request
from urllib.error import HTTPError
from datetime import datetime, timedelta
from singer.schema import Schema


def get_abs_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)


def load_schemas(schema_folder):
    """ Load schemas from schemas folder """
    schemas = {}
    for filename in os.listdir(get_abs_path(schema_folder)):
        if filename.endswith('json'):
            path = get_abs_path(schema_folder) + '/' + filename
            file_raw = filename.replace('.json', '')
            with open(path) as file:
                schemas[file_raw] = Schema.from_dict(json.load(file))
                print(schemas[file_raw])
    return schemas


def main(config_file, schema_folder):
    # load configuration file
    with open(config_file, encoding='utf-8') as config_file:
        config = json.load(config_file)
        HEADERS = config['headers']
        ENDPOINTS = config['data']

    # load all wunder schemas
    schemas = load_schemas(schema_folder)

    # make all api requests
    for schema_name, endpoint in ENDPOINTS.items():

        singer.write_schema(schema_name,
                            schemas.get(schema_name).to_dict(),  # needs to be turned to dict (not in template)
                            endpoint['primary_key'])

        request_url = endpoint['endpoint']

        yesterday = datetime.now() - timedelta(days=1)  # TODO: change again to one day
        if schema_name in ['reservations']:
            request_url = request_url + '?endTime_after=' + yesterday.strftime("%Y-%m-%d")
        elif schema_name in ['customers']:
            request_url = request_url + '?registrationDate_after=' + yesterday.strftime("%Y-%m-%d")

        try:
            req = urllib.request.Request(request_url,
                                         headers=HEADERS,
                                         method='POST')

            with urllib.request.urlopen(
                    req) as response:
                data = json.loads(response.read().decode())

                singer.write_records(schema_name, data)

        except HTTPError as err:
            print("HTTP Error. {0}".format(err))


if __name__ == "__main__":
    main()


# code from template https://github.com/singer-io/singer-tap-template.git

"""

from singer import utils, metadata
from singer.catalog import Catalog, CatalogEntry

def discover():
    raw_schemas = load_schemas()
    streams = []
    for stream_id, schema in raw_schemas.items():
        # TODO: populate any metadata and stream's key properties here..
        stream_metadata = []
        key_properties = []
        streams.append(
            CatalogEntry(
                tap_stream_id=stream_id,
                stream=stream_id,
                schema=schema,
                key_properties=key_properties,
                metadata=stream_metadata,
                replication_key=None,
                is_view=None,
                database=None,
                table=None,
                row_count=None,
                stream_alias=None,
                replication_method=None,
            )
        )
    return Catalog(streams)

def sync(config, state, catalog):
    # Loop over selected streams in catalog
    for stream in catalog.get_selected_streams(state):
        LOGGER.info("Syncing stream:" + stream.tap_stream_id)

        bookmark_column = stream.replication_key
        is_sorted = True  # TODO: indicate whether data is sorted ascending on bookmark value

        singer.write_schema(
            stream_name=stream.tap_stream_id,
            schema=stream.schema,
            key_properties=stream.key_properties,
        )

        # TODO: delete and replace this inline function with your own data retrieval process:
        tap_data = lambda: [{"id": x, "name": "row${x}"} for x in range(1000)]

        max_bookmark = None
        for row in tap_data():
            # TODO: place type conversions or transformations here

            # write one or more rows to the stream:
            singer.write_records(stream.tap_stream_id, [row])
            if bookmark_column:
                if is_sorted:
                    # update bookmark to latest value
                    singer.write_state({stream.tap_stream_id: row[bookmark_column]})
                else:
                    # if data unsorted, save max value until end of writes
                    max_bookmark = max(max_bookmark, row[bookmark_column])
        if bookmark_column and not is_sorted:
            singer.write_state({stream.tap_stream_id: max_bookmark})
    return

@utils.handle_top_exception(LOGGER)
def main():
    # Parse command line arguments
    args = utils.parse_args(REQUIRED_CONFIG_KEYS)

    # If discover flag was passed, run discovery mode and dump output to stdout
    if args.discover:
        catalog = discover()
        catalog.dump()
    # Otherwise run in sync mode
    else:
        if args.catalog:
            catalog = args.catalog
        else:
            catalog = discover()
        sync(args.config, args.state, catalog)
"""

